const MAKE_REQUEST = 'MAKE_REQUEST';
const SUCCESS_RESPONSE = 'SUCCESS_RESPONSE';
const ERROR_RESPONSE = 'ERROR_RESPONSE';
const SET_SKIP = 'SET_SKIP';

const initialState = {
  todos: [],
  total: 0,
  skip: 0,
  loading: false,
  error: null,
};

export function makeInitialState(skip) {
  return {
    ...initialState,
    skip,
  };
}

export function todoReducer(state = initialState, action) {
  switch (action.type) {
    case MAKE_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case SUCCESS_RESPONSE:
      return {
        todos: action.payload.todos,
        total: action.payload.total,
        skip: action.payload.skip,
        loading: false,
        error: null,
      };
    case ERROR_RESPONSE:
      return {
        todos: [],
        total: 0,
        skip: 0,
        loading: false,
        error: action.payload,
      };
    case SET_SKIP:
      return {
        ...state,
        skip: action.payload,
      };
    default:
      return { ...state };
  }
}

export function makeRequest() {
  return {
    type: MAKE_REQUEST,
  };
}

export function successResponse(payload) {
  return {
    type: SUCCESS_RESPONSE,
    payload,
  };
}

export function errorResponse(payload) {
  return {
    type: ERROR_RESPONSE,
    payload,
  };
}

export function setSkip(payload) {
  return {
    type: SET_SKIP,
    payload,
  };
}
