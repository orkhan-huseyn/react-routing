import { createContext, useContext, useEffect, useReducer } from 'react';
import { useSearchParams } from 'react-router-dom';
import {
  errorResponse,
  makeRequest,
  successResponse,
  todoReducer,
  makeInitialState,
  setSkip,
} from './todoReducer';

const TODOS_URL = import.meta.env.VITE_BACKEND_URL;
const LIMIT = 30;

const TodoContext = createContext();

export function useTodoContext() {
  return useContext(TodoContext);
}

function TodoProvider({ children }) {
  let pageFromURL = 1;

  const [searchParams, setSearchParams] = useSearchParams();

  if (searchParams.has('page')) {
    pageFromURL = Number(searchParams.get('page'));
  }

  const [state, dispatch] = useReducer(
    todoReducer,
    makeInitialState((pageFromURL - 1) * LIMIT)
  );

  useEffect(() => {
    dispatch(makeRequest());
    fetch(TODOS_URL + state.skip)
      .then((res) => res.json())
      .then((res) => {
        dispatch(successResponse(res));
      })
      .catch((err) => {
        dispatch(errorResponse(err));
      });
  }, [state.skip]);

  function handlePageChange(page) {
    dispatch(setSkip((page - 1) * LIMIT));
    setSearchParams({ page });
  }

  const contextValue = [
    state.todos,
    state.loading,
    state.total,
    state.skip / LIMIT + 1,
    handlePageChange,
  ];

  return (
    <TodoContext.Provider value={contextValue}>{children}</TodoContext.Provider>
  );
}

export default TodoProvider;
