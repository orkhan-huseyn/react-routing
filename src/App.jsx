import React from 'react';
import { Routes, Route } from 'react-router-dom';

import TodoProvider from './contexts/todoContext';
import Layout from './components/Layout';

const HomePage = React.lazy(() => import('./pages/HomePage'));
const AboutPage = React.lazy(() => import('./pages/AboutPage'));
const ContactPage = React.lazy(() => import('./pages/ContactPage'));
const TodosPage = React.lazy(() => import('./pages/TodosPage'));
const SingleTodo = React.lazy(() => import('./pages/SingleTodo'));

function App() {
  return (
    <TodoProvider>
      <React.Suspense fallback={<h1>Loading...</h1>}>
        <Routes>
          <Route element={<Layout />}>
            <Route index element={<HomePage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/contact" element={<ContactPage />} />
            <Route path="/todos" element={<TodosPage />} />
            <Route path="/todos/:todoId" element={<SingleTodo />} />
          </Route>
        </Routes>
      </React.Suspense>
    </TodoProvider>
  );
}

export default App;
