import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';

function SingleTodo() {
  const { todoId } = useParams();
  const [todo, setTodo] = useState();
  const [user, setUser] = useState();

  useEffect(() => {
    fetch('https://dummyjson.com/todos/' + todoId)
      .then((res) => res.json())
      .then((res) => {
        setTodo(res);
      });
  }, []);

  useEffect(() => {
    if (!todo) return;
    fetch('https://dummyjson.com/users/' + todo.userId)
      .then((res) => res.json())
      .then((res) => {
        setUser(res);
      });
  }, [todo]);

  if (!todo) {
    return <h1>Fetching info...</h1>;
  }

  return (
    <>
      <Link to="/todos">⬅️ Back</Link>
      <h1>{todo.todo}</h1> {todo.completed ? '✅' : ''}
      {user ? <span>User: @{user.username}</span> : null}
    </>
  );
}

export default SingleTodo;
