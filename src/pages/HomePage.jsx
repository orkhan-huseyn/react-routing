import { useTodoContext } from '../contexts/todoContext';

function HomePage() {
  const [todos] = useTodoContext();

  const uncompletedTasks = todos.filter((todo) => !todo.completed).length;

  return <h1>Home page {uncompletedTasks}</h1>;
}

export default HomePage;
