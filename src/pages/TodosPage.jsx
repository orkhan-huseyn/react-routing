import { Link } from 'react-router-dom';
import { useTodoContext } from '../contexts/todoContext';

import Pagintion from '../components/Pagination';

const LIMIT = 30;

function TodosPage() {
  const [todos, loading, total, currentPage, handlePageChange] =
    useTodoContext();

  if (loading) {
    return <h1>Fetching todos...</h1>;
  }

  return (
    <>
      <h1>Todo list</h1>
      <ul>
        {todos.map((todo) => (
          <li key={todo.id}>
            {todo.todo} {todo.completed ? '✅' : ''}{' '}
            <Link to={`/todos/${todo.id}`}>Read more</Link>
          </li>
        ))}
      </ul>
      <Pagintion
        total={total}
        pageSize={LIMIT}
        onPageChange={handlePageChange}
        defaultCurrentPage={currentPage}
      />
    </>
  );
}

export default TodosPage;
