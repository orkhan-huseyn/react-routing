import { useNavigate } from 'react-router-dom';

function ContactPage() {
  const navigate = useNavigate();

  function handleSubmit(event) {
    event.preventDefault();
    // validation
    // send to back
    const success = true;
    if (success) {
      navigate('/about');
    } else {
      // set error
    }
  }

  return (
    <>
      <h1>Contact page</h1>
      <form onSubmit={handleSubmit}>
        <input type="email" placeholder="Email" />
        <textarea placeholder="Text..."></textarea>
        <button>Send</button>
      </form>
    </>
  );
}

export default ContactPage;
