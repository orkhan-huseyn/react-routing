import { NavLink, Outlet } from 'react-router-dom';

function Layout() {
  return (
    <>
      <header>
        <NavLink to="/">Home page</NavLink>

        <nav>
          <NavLink to="/about">About</NavLink>
          <NavLink to="/contact">Contact</NavLink>
          <NavLink to="/todos">Todos</NavLink>
        </nav>
      </header>

      <main>
        <Outlet />
      </main>

      <footer>
        <small>Copyright 2023</small>
      </footer>
    </>
  );
}

export default Layout;
